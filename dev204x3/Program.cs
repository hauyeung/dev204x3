﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dev204x3
{
    class Program
    {
        public static string _firstName;
        public static string _lastName;
        public static string _birthday;
        public static string _course;
        public static string _program;
        public static string _degree;
        static void Main(string[] args)
        {
            GetStudentInfo();
            PrintStudentDetails(getFirstName(), getLastName(), getBirthday(), getCourse(), getProgram(), getDegree());

        }

        static string getFirstName()
        {
            return _firstName;
        }

        static string getLastName()
        {
            return _lastName;
        }

        static string getBirthday()
        {
            return _birthday;
        }

        static string getCourse()
        {
            return _course;
        }

        static string getProgram()
        {
            return _program;
        }

        static string getDegree()
        {
            return _degree;
        }
        static void GetStudentInfo()
        {
            Console.WriteLine("Enter the student's first name: ");
            string firstName = Console.ReadLine();
            _firstName = firstName;
            Console.WriteLine("Enter the student's last name");
            string lastName = Console.ReadLine();
            _lastName = lastName;
            // Code to finish getting the rest of the student data
            Console.WriteLine("Enter the student's birthday");
            string birthday = Console.ReadLine();
            _birthday = birthday;
            Console.WriteLine("Enter the student's course");
            string course = Console.ReadLine();
            _course = course;
            Console.WriteLine("Enter the student's program");
            string program = Console.ReadLine();
            _program = program;
            Console.WriteLine("Enter the student's degree");
            string degree = Console.ReadLine();
            _degree = degree;
        }

        static void PrintStudentDetails(string first, string last, string birthday, string course, string program, string degree)
        {
            Console.WriteLine("{0} {1} was born on: {2}", first, last, birthday);
            Console.WriteLine("(S)he is taking: {0}, {1}, {2}", course, program, degree);
            Console.ReadKey();
        }
    }
}

